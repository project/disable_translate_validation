<?php

/**
 * @file
 * Disables translate validation for individual strings.
 */

/**
 * Implements hook_help().
 */
function disable_translate_validation_help($path, $arg) {
  switch ($path) {
    case 'admin/help#disable_translate_validation':
      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }
      return $output;
  }
}

/**
 * Implements hook_form_i18n_string_locale_translate_edit_form_alter().
 */
function disable_translate_validation_form_i18n_string_locale_translate_edit_form_alter(&$form, &$form_state, $form_id) {
  $form['disable_validation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable validation'),
  );
  unset($form['#validate']);
  $form['#validate'][] = 'disable_translate_validation_handler';
}

/**
 * Custom validation handler for disable_translate_validation_form_i18n_string_locale_translate_edit_form_alter.
 */
function disable_translate_validation_handler(&$form, &$form_state) {
  if ($form_state['values']['disable_validation'] != TRUE) {
    // Run the default checks if disable translation validation is not checked.
    if (empty($form_state['values']['i18n_string_context']) || empty($form_state['values']['i18n_string_context']->format)) {
      $copy_state = $form_state;
      $copy_state['values']['textgroup'] = 'default';
      module_load_include('admin.inc', 'locale');
      locale_translate_edit_form_validate($form, $copy_state);
    }
    elseif (!i18n_string_translate_access($form_state['values']['i18n_string_context'])) {
      form_set_error('translations', t('You are not allowed to translate or edit texts with this text format.'));
    }
  }
}
