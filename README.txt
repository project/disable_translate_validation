INTRODUCTION
------------

If attempts to translate strings containing certain HTML tags cause the `The
submitted string contains disallowed HTML` error, then most probably you just
have not checked on yet neither the Filtered HTML nor Full HTML on the
`/admin/config/regional/i18n/strings` page, which come disabled by default.
However, if the problem persists even after enabling needed filter(s), then you
might need this module.

We didn't file this as a bug, because in most cases it probably works as
designed. For example, if you are trying to translate strings which originate
from Views, then enabling the Internationalization Views could be your solution.
However, sometimes enabling additional modules in order to just one or couple
strings get translated could be an overkill, so there this module comes really
handy.


REQUIREMENTS
------------

This module requires the following modules:

 * Internationalization (https://www.drupal.org/project/i18n)
 * String translation (https://www.drupal.org/project/i18n)
 * Translation sets (https://www.drupal.org/project/i18n)


 INSTALLATION
 ------------

 1. Download and extract the module to the 'sites/all/modules/' folder.
 2. If updating, run the update.php script following the standard procedure for
    Drupal updates.


CONFIGURATION
------------

1. Go to the <code>/admin/config/regional/translate/translate</code> and enter
   the string to translate, which usually gives the above-described error;
2. Click on "Edit" and on the next page you will notice a new checkbox called
   "Disable validation" just above the "Save translations" button, check it on.
3. Try to save your translation and this time it will pass.
